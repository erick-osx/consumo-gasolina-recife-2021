Consumo Gasolina
===========

# django-simples

Exemplo de um projeto simples com Django.

## Como rodar o projeto?

* Clone esse repositório.
* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.
* Banco de dados Postgres disponível;
* As configurações do banco de dados estão no arquivo consumo_gasolina.conf.
* Rode as migrações.

```
git clone https://gitlab.com/chiavegatto/consumo-gasolina-devops.git
consumo-gasolina-devops
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python manage.py migrate
```

## Como rodar o projeto com Docker?

* Clone esse repositório.
* Faça o build da imagem
* Altere o docker-compose.yml para rodar com a sua imagem.

```
git clone https://gitlab.com/chiavegatto/consumo-gasolina-devops.git
consumo-gasolina-devops
docker image build suaimagem .
alterar o docker-compose.yml
docker-compose up -d
```